#!/bin/bash
set -e

# envsubst < /tmp/matomo-config/httpdcern10.conf > /etc/apache2/conf-available/httpdcern10.conf

# Enabling Apache2 configuration
# a2enconf httpdcern10

exec apache2-foreground
