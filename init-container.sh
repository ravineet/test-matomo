#!/bin/bash
set -e

echo "--> Setting up configurations ..."
rsync -rogv --exclude 'config.ini.php' /usr/src/matomo/config/ /var/www/html/config/
echo "--> Setting up configurations ... [DONE]"

# Update Matomo database in case there is a new version.
if [ -f /var/www/html/config/config.ini.php ];
then php console core:update --yes;
fi
